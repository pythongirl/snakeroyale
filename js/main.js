/* global createREGL */

const regl = createREGL()

const draw_frame = regl({
  frag: `
  precision mediump float;
  uniform sampler2D map;
  uniform vec2 resolution;
  uniform mat3 viewtransform;
  void main () {
		vec3 ipos = vec3(gl_FragCoord.xy, 1.);
    vec3 pos = viewtransform * ipos * 16.;

    vec3 color = vec3(1.);
    if(mod(pos.x, 8.) > 7. || mod(pos.y, 8.) > 7.){
      color = vec3(0.);
    }
    gl_FragColor = vec4(color,1.);
  }`,

  vert: `
  precision mediump float;
  attribute vec2 position;
  void main () {
    gl_Position = vec4(position, 0, 1);
  }`,

  attributes: {
    position: [-3, -1, 1, -1, 1, 3] // One triangle to cover entire screen
  },

  uniforms: {
    map: regl.texture([
      [[255, 255, 255, 255], [0, 255, 0, 255], [0, 255, 0, 255], [0, 255, 0, 255]],
      [[255, 255, 255, 255], [255, 255, 255, 255], [255, 0, 0, 255], [255, 255, 255, 255]]
    ]),
    resolution: (context) => [context.framebufferWidth, context.framebufferHeight],

    viewtransform: (context) => {
      let r = {x: context.framebufferWidth, y: context.framebufferHeight}
      let r_min = Math.min(r.x, r.y)
      let r2 = {x: r.x / 2, y: r.y / 2}

      return [
        1 / r_min, 0, 0,
        0, 1 / -r_min, 0,
        -r2.x / r_min, r2.y / r_min + 1, 1
      ]
    }
  },

  count: 3
})

regl.frame(() => {
  regl.clear({ color: [1,1,1,1] })

  draw_frame()
})