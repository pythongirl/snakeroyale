/* global Peer */

let peer = new Peer(null, {
  debug: 2
})

window.peer = peer

let Console = console

peer.on("error", err => {
  // Console.error("PeerJS Error:", err)
})

peer.on("open", id => {
  Console.log("Opened")
  Console.log("id:", peer.id)
})

peer.on("connection", conn => {
  Console.log("Connected")

  conn.on("data", data => {
    Console.log("Data recieved:", data)
  })

  window.conn = conn
})

function connect(id){
  let conn = peer.connect(id, {
    reliable: false
  })
  window.conn = conn

  conn.on("data", data => {
    Console.log("Data recieved (initiator):", data)
  })
}

window.connect = connect